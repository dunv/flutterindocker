FROM debian:bullseye AS frontend_builder
RUN apt-get update
# RUN apt-get install -y curl git wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback lib32stdc++6 python3
RUN apt-get install -y curl git wget unzip libgconf-2-4 gdb libstdc++6 libglu1-mesa fonts-droid-fallback python3

ARG FLUTTER_VERSION
RUN git clone https://github.com/flutter/flutter.git /usr/local/flutter -b $FLUTTER_VERSION
ENV PATH="/usr/local/flutter/bin:/usr/local/flutter/bin/cache/dart-sdk/bin:${PATH}"

WORKDIR /usr/local/flutter

RUN flutter config --enable-web && \
    flutter precache && \
    flutter doctor -v
